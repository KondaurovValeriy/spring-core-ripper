package quoters;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
public class ConfigBean {

    @Bean
    public TerminatorQuoterImpl terminatorQuoter() {
        return new TerminatorQuoterImpl();
    }
}
