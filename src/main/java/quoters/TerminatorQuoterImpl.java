package quoters;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;


public class TerminatorQuoterImpl implements Quoter {

    @Value("I'll be back")
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void sayQuote() {
        System.out.println(message);
    }
}
